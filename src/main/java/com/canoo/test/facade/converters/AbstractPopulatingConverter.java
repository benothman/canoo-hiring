/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.converters;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;

import com.canoo.test.facade.populators.Populator;
import com.canoo.test.facade.populators.PopulatorList;

/**
 * Populating converter that uses a list of configured populators to populate
 * the target during conversion. Class used to be but is no longer abstract. It
 * allows to declare it as an abstract bean in Spring, otherwise we'd get
 * BeanInstantiationException.
 */
public class AbstractPopulatingConverter<S, T> extends AbstractConverter<S, T> implements PopulatorList<S, T> {

	/**
	 *  
	 */
	private List<Populator<S, T>> populators;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.canoo.test.facade.converters.AbstractConverter#populate(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void populate(final S source, final T target) {
		final List<Populator<S, T>> list = getPopulators();

		if (list == null || list.isEmpty()) {
			return;
		}

		list.stream().filter(p -> p != null).forEach(populator -> populator.populate(source, target));
	}

	// execute when BEAN name is known
	@PostConstruct
	public void removePopulatorsDuplicates() {
		// CHECK for populators duplicates
		if (CollectionUtils.isEmpty(populators)) {
			getLogger().warn("Empty populators list found for converter {}!", getBeanName());
			return;
		}

		final Set<Populator<S, T>> distinctPopulators = new LinkedHashSet<>();

		populators.forEach(populator -> {
			if (!distinctPopulators.add(populator)) {
				getLogger().warn(
						"Duplicate populator entry [{}] found for converter {}! The duplication has been removed.",
						populator.getClass().getName(), getBeanName());
			}
		});

		this.populators = new ArrayList<>(distinctPopulators);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.populators.PopulatorList#getPopulators()
	 */
	@Override
	public List<Populator<S, T>> getPopulators() {
		return this.populators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.canoo.test.facade.populators.PopulatorList#setPopulators(java.util.List)
	 */
	@Override
	public void setPopulators(final List<Populator<S, T>> populators) {
		this.populators = populators;
	}
}
