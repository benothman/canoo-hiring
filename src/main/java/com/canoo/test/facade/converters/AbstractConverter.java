/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.converters;

import java.lang.reflect.Method;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.ReflectionUtils;

import com.canoo.test.facade.populators.Populator;

import lombok.Getter;
import lombok.Setter;

/**
 * Abstract implementation of the Converter interface that also supports the
 * Populator interface. Implementations of this base type can either be used as
 * a converter or as a populator. When used as a converter the
 * {@link #createTarget()} method is called to create the target instance and
 * then the {@link #populate(Object, Object)} method is called to populate the
 * target with values from the source instance. The {@link #createTarget()}
 * method can be implemented via a spring <tt>lookup-method</tt> rather than
 * being overridden in code.
 */
@Getter
@Setter
public abstract class AbstractConverter<S, T>
		implements Converter<S, T>, Populator<S, T>, InitializingBean, BeanNameAware {

	/**
	 * 
	 */
	private Class<T> targetClass;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.converters.Converter#convert(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public T convert(final S source, final T target) throws ConversionException {
		populate(source, target);
		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public T convert(final S source) throws ConversionException {
		if (source == null) {
			return null;
		}
		final T target = targetClass == null ? createTarget() : createFromClass();
		populate(source, target);
		return target;
	}

	/**
	 * Populate the target instance from the source instance. Calls a list of
	 * injected populators to populate the instance.
	 * 
	 * @param source the source item
	 * @param target the target item to populate
	 * @see #setTargetClass(Class)
	 */
	@Override
	public abstract void populate(final S source, final T target);

	/**
	 * Allows to specify the target object class directly. Please use that instead
	 * of the deprecated <lookup-method name="createTarget" ref="bean"> approach, as
	 * it's way faster.
	 */
	public void setTargetClass(final Class<T> targetClass) {
		this.targetClass = targetClass;

		// sanity check - can we instantiate that class ?
		if (targetClass != null) {
			createFromClass();
		}
	}

	public T createFromClass() {
		try {
			return targetClass.newInstance();
		} catch (final InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @deprecated please inject the target class directly, since it's way faster
	 *             than the Spring <lookup-method> approach!
	 */
	@Deprecated
	public T createTarget() {
		// optional - no longer requiring sub classes to implement this method
		throw new NotImplementedException("Method not implemented!");
	}

	// -------------------------------------------------------------------
	// --- Sanity check for the two different converter setups
	// -------------------------------------------------------------------

	private String beanName;

	/*
	 * Ensures that either a class has been set or createTarget() has been
	 * overridden
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		if (targetClass == null) {
			final Class<? extends AbstractConverter> cl = this.getClass();
			final Method createTargetMethod = ReflectionUtils.findMethod(cl, "createTarget");
			if (AbstractConverter.class.equals(createTargetMethod.getDeclaringClass())) {
				throw new IllegalStateException("Converter '" + beanName
						+ "' doesn't have a targetClass property nor does it override createTarget()!");
			}
		}
	}
}
