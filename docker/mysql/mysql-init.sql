-- creating the common user
CREATE USER IF NOT EXISTS canoo_usr IDENTIFIED BY 'canoo123456';
-- creating the different databases
CREATE DATABASE canoolibrary  CHARACTER SET utf8 COLLATE utf8_bin;

-- granting priveleges on all databases to the common user
GRANT ALL PRIVILEGES ON canoolibrary.*  TO canoo_usr@'%' IDENTIFIED BY 'canoo123456';
