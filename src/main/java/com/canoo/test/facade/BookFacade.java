/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.canoo.test.facade.data.BookData;

/**
 * {@code BookFacade}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 19:46:31
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public interface BookFacade {

	/**
	 * Search for the books meeting the search query.
	 * 
	 * @param pageable the pagination data
	 * @return a {@code Page} of {@code BookData}
	 */
	public Page<BookData> search(final String query, final Pageable pageable);

	/**
	 * Find the books in the database.
	 * 
	 * @param pageable the pagination data
	 * @return a {@code Page} of {@code BookData}
	 */
	public Page<BookData> findAll(final Pageable pageable);

	/**
	 * Find the book by its {@literal PK}
	 * 
	 * @param id the book PK
	 * @return the book having the specified PK
	 */
	public BookData getForID(final long id);

	/**
	 * Find the book by its ISBN number
	 * 
	 * @param isbn the book ISBN number
	 * @return the book having the specified ISBN number
	 */
	public BookData findByIsbn(final String isbn);

	/**
	 * Find the books written by the specified author.
	 * 
	 * @param author
	 * @return
	 */
	public List<BookData> findByAuthor(final String author);

	/**
	 * Find the list of book having the specified title
	 * 
	 * @param title
	 * @return
	 */
	public List<BookData> findByTitle(final String title);

	/**
	 * 
	 * @param book
	 * @return
	 */
	public BookData create(final BookData book);

	/**
	 * 
	 * @param book
	 * @return
	 */
	public BookData update(final BookData book);

	/**
	 * Delete the book
	 * 
	 * @param book
	 */
	public void delete(final BookData book);

}
