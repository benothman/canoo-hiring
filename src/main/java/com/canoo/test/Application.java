/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.canoo.test.core.model.Book;
import com.canoo.test.core.model.User;
import com.canoo.test.core.model.UserRole;
import com.canoo.test.core.repository.BookRepository;
import com.canoo.test.core.repository.UserRepository;

/**
 * {@code Application}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 18:08:52
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@SpringBootApplication
@ConfigurationProperties
@EnableAutoConfiguration(exclude = { WebMvcAutoConfiguration.class, ErrorMvcAutoConfiguration.class })
@EnableConfigurationProperties
@EnableScheduling
@ImportResource("classpath:application-context.xml")
public class Application extends SpringBootServletInitializer {

	/**
	 * 
	 */
	private final static Logger LOG = LogManager.getLogger(Application.class);

	/**
	 * 
	 * @param args
	 */
	public static void main(final String... args) {
		LOG.info("Starting canoo-library application");
		var applicationContext = SpringApplication.run(Application.class, args);
		applicationContext.registerShutdownHook();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.boot.web.servlet.support.SpringBootServletInitializer#
	 * configure(org.springframework.boot.builder.SpringApplicationBuilder)
	 */
	@Override
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	@Bean
	protected CommandLineRunner run( //
			@Autowired final UserRepository userRepository, //
			@Autowired final BookRepository bookRepository, //
			@Autowired final PasswordEncoder passwordEncoder) {
		return (args) -> {
			if (!userRepository.findByUsername("admin").isPresent()) {
				LOG.info("creating admin user");
				var admin = createUser("Admin User", "admin", "admin@book-library.com", passwordEncoder.encode("nimda"),
						UserRole.ROLE_ADMIN);
				userRepository.save(admin);
			}
			if (!userRepository.findByUsername("user1").isPresent()) {
				LOG.info("creating normal user1");
				var user1 = createUser("Normal User #1", "user1", "user1@book-library.com",
						passwordEncoder.encode("mypass1"), UserRole.ROLE_USER);
				userRepository.save(user1);
			}
			if (!userRepository.findByUsername("user2").isPresent()) {
				LOG.info("creating normal user2");
				var user2 = createUser("Normal User #2", "user2", "user2@book-library.com",
						passwordEncoder.encode("mypass2"), UserRole.ROLE_USER);
				userRepository.save(user2);
			}

			var admin = userRepository.findByUsername("admin").get();
			var user1 = userRepository.findByUsername("user1").get();
			var user2 = userRepository.findByUsername("user2").get();

			for (int i = 0; i < 10; i++) {
				bookRepository.save(createBook(admin));
				bookRepository.save(createBook(user1));
				bookRepository.save(createBook(user2));
			}
		};
	}

	private Book createBook(final User addedBy) {
		var book = new Book();
		var random = new Random();

		var authors = new String[] { "Me", "François Dupont", "James Goslin", "Steve Jobs", "Toto Titi", "Ali Baba",
				"Michael Jackson" };
		book.setAuthor(authors[Math.abs(random.nextInt(authors.length))]);
		book.setAddedBy(addedBy);
		var number = random.nextLong();
		book.setIsbn("ISBN-" + number);

		var titles = new String[] { "Spring Boot in action", "Java for nulls", "Java SE 8", "Scala 2",
				"Kotlin in action", "SAP Hybris", "Cassandra NoSQL" };

		book.setTitle(titles[Math.abs(random.nextInt(titles.length))] + " - version" + random.nextInt(10));
		book.setLanguage("English");
		book.setDescription("This is a very nice book to read about " + book.getTitle());
		return book;
	}

	private User createUser(final String name, final String username, final String email, final String password,
			final UserRole role) {
		var user = new User();
		user.setUsername(username);
		user.setEmail(email);
		user.setRole(role);
		user.setName(name);
		user.setPassword(password);
		return user;
	}

}
