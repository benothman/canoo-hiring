/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.model;

import java.io.Serializable;
import java.util.Date;

/**
 * {@code PersistentEntity}
 * <p/>
 * 
 * @param <PK>
 *            Created on 27 Aug 2018 at 18:20:37
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public interface PersistentEntity<PK extends Serializable> extends Serializable {
	
	/**
	 * @return the value of the {@code id}
	 */
	public PK getId();
	
	/**
	 * @return the creation timestamp
	 */
	public Date getCreateTime();
	
	/**
	 * @return the update timestamp
	 */
	public Date getModifiedTime();
	
	/**
	 * Set a new value to the {@code id}
	 *
	 * @param id
	 *            the {@code id} to set
	 */
	public void setId(final PK id);
	
	/**
	 * @return {@code true} if the entity is newly created and does not have yet a
	 *         mapping in the data store
	 */
	public boolean isNew();	
}
