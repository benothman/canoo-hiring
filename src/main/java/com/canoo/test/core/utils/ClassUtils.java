/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.utils;

import java.lang.reflect.Field;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

/**
 * {@code ClassUtils}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 00:22:49
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public final class ClassUtils {

	/**
	 * Creates new instance of {@code ClassUtils}
	 */
	private ClassUtils() {
		super();
	}

	/**
	 * Checks whether the target {@code Class} or one of its super classes has a
	 * declared filed with the specified name. If either {@code clazz} is
	 * {@literal null} or {@code name} is blank, then it returns {@literal false}.
	 * 
	 * @param clazz the target class.
	 * @param name  the name of the field
	 * @return {@literal true} if the target {@code Class} or one of its super
	 *         classes has a declared filed with the specified name.
	 */
	public static boolean hasField(final Class<?> clazz, final String name) {
		return StringUtils.isNotBlank(name) && hasField0(clazz, name);
	}

	/**
	 * Checks whether the target {@code Class} or one of its super classes has a
	 * declared filed with the specified name.
	 * 
	 * @param clazz the target class.
	 * @param name  the name of the field
	 * @return {@literal true} if the target {@code Class} or one of its super
	 *         classes has a declared filed with the specified name.
	 */
	private static boolean hasField0(final Class<?> clazz, final String name) {
		return clazz != null && //
				(Stream.of(clazz.getDeclaredFields()).map(Field::getName).anyMatch(name::equals)
						|| hasField0(clazz.getSuperclass(), name));
	}
}
