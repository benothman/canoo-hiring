/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.canoo.test.core.model.Book;

/**
 * {@code BookRepository}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 18:30:41
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public interface BookRepository extends JpaRepository<Book, Long> {

	/**
	 * Find the book by its ISBN number
	 * 
	 * @param isbn the book ISBN number
	 * @return the book having the specified ISBN number
	 */
	public Book findByIsbn(final String isbn);

	/**
	 * Find the books written by the specified author.
	 * 
	 * @param author the books author.
	 * @return the list of books written by the specified author.
	 */
	public List<Book> findByAuthor(final String author);

	/**
	 * Find the list of book having the specified title.
	 * 
	 * @param title the books title
	 * @return a list of books having the specified title.
	 */
	public List<Book> findByTitle(final String title);

	/**
	 * Search for the books meeting the following criteria:
	 * <ul>
	 * <li>The title contains the search query</li>
	 * <li>The ISBN contains the search query</li>
	 * <li>The author contains the search query</li>
	 * <li>The description contains the search query</li>
	 * </ul>
	 * 
	 * @param query    the search query.
	 * @param pageable the pagination data.
	 * @return {@code Page} of {@code Book}
	 */
	@Query("SELECT o FROM Book o WHERE LOWER(o.title) LIKE %:q% OR LOWER(o.isbn) LIKE %:q% OR LOWER(o.author) LIKE %:q% OR LOWER(o.description) LIKE %:q%")
	public Page<Book> search(@Param("q") final String query, final Pageable pageable);

}
