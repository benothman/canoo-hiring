/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.web;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.canoo.test.core.model.Book;
import com.canoo.test.core.utils.ClassUtils;
import com.canoo.test.facade.BookFacade;
import com.canoo.test.facade.data.BookData;

import lombok.Getter;
import lombok.Setter;

/**
 * {@code BookController}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 19:46:31
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@Getter
@Setter
@RestController("BookController")
@RequestMapping("/books")
//@RolesAllowed({ "ADMIN", "ROLE_ADMIN", "USER", "ROLE_USER" })
public class BookController {

	@Autowired
	private BookFacade bookFacade;

	/**
	 * 
	 * @param query
	 * @param page
	 * @param size
	 * @param direction
	 * @param orderBy
	 * @return
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Page<BookData>> getPaged(
			@RequestParam(name = "q", required = false, defaultValue = "") final String query,
			@RequestParam(name = "page", required = false, defaultValue = "0") final int page,
			@RequestParam(name = "size", required = false, defaultValue = "10") final int size,
			@RequestParam(name = "direction", required = false, defaultValue = "ASC") final String direction,
			@RequestParam(name = "orderBy", required = false, defaultValue = "title") final String orderBy) {

		return ResponseEntity.ok(bookFacade.search(query, createPageable(page, size, direction, orderBy, Book.class)));
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = { "/{id:[0-9]+}" }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<BookData> getByPK(@PathVariable("id") final long id) {
		var book = bookFacade.getForID(id);
		return book == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(book);
	}

	/**
	 * 
	 * @param isbn
	 * @return
	 */
	@GetMapping(value = { "/{isbn:.*}" }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<BookData> getByISBN(@PathVariable("isbn") final String isbn) {
		var book = bookFacade.findByIsbn(isbn);
		return book == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(book);
	}

	/**
	 * 
	 * @param book
	 * @return
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<BookData> post(@RequestBody final BookData book) {
		try {
			return ResponseEntity.ok(bookFacade.create(book));
		} catch (final Exception exp) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}

	/**
	 * 
	 * @param book
	 * @param id
	 * @return
	 */
	@PutMapping(value = "/{id:[0-9]+}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<BookData> put(@RequestBody final BookData book, @PathVariable("id") final long id) {
		if (book.getId() != id) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		return ResponseEntity.ok(bookFacade.update(book));
	}

	/**
	 * Delete operation is allowed only for ADMIN users.
	 * 
	 * @param id the book ID
	 * @return
	 */
	@Secured({ "ROLE_ADMIN" })
	@DeleteMapping("/{id:[0-9]+}")
	public ResponseEntity<Void> delete(@PathVariable("id") final long id) {
		var book = bookFacade.getForID(id);
		if (book != null) {
			bookFacade.delete(book);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * 
	 * @param page
	 * @param size
	 * @param sortDirection
	 * @param orderByStr
	 * @param targetClass
	 * @return
	 */
	protected Pageable createPageable(final int page, final int size, final String sortDirection,
			final String orderByStr, final Class<?> targetClass) {
		// Page size must be between 10 and 100
		var newSize = Math.min(Math.max(size, 10), 100);
		// Page number must be positive
		var newPage = Math.max(page, 0);
		// Sort direction
		var direction = StringUtils.equalsIgnoreCase(sortDirection, "DESC") ? Direction.DESC : Direction.ASC;
		// sorting by properties
		var orderBy = StringUtils.defaultIfBlank(orderByStr, StringUtils.EMPTY).split(",");

		var validProps = Stream.<String>of(orderBy).filter(s -> s != null)
				.filter(name -> ClassUtils.hasField(targetClass, name)).collect(Collectors.toList());

		return validProps.isEmpty() ? PageRequest.of(newPage, newSize)
				: PageRequest.of(newPage, newSize, direction, validProps.toArray(new String[validProps.size()]));
	}

}
