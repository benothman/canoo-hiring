/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.canoo.test.core.model.Book;
import com.canoo.test.core.repository.BookRepository;
import com.canoo.test.core.repository.UserRepository;
import com.canoo.test.facade.BookFacade;
import com.canoo.test.facade.converters.Converter;
import com.canoo.test.facade.data.BookData;
import com.canoo.test.facade.populators.Populator;

import lombok.Getter;
import lombok.Setter;

/**
 * {@code DefaultBookFacade}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 19:46:31
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@Getter
@Setter
@Component("defaultBookFacade")
public class DefaultBookFacade implements BookFacade {

	private static final Logger LOG = LogManager.getLogger(DefaultBookFacade.class);

	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private Converter<Book, BookData> bookConverter;
	@Autowired
	private Populator<BookData, Book> reverseBookPopulator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.BookFacade#search(java.lang.String,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<BookData> search(final String query, final Pageable pageable) {
		if (StringUtils.isBlank(query)) {
			return findAll(pageable);
		}

		return bookRepository.search(query.toLowerCase(), pageable).map(bookConverter::convert);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.canoo.test.facade.BookFacade#findAll(org.springframework.data.domain.
	 * Pageable)
	 */
	@Override
	public Page<BookData> findAll(final Pageable pageable) {
		var bookPage = bookRepository.findAll(pageable);
		return bookPage.map(bookConverter::convert);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.BookFacade#getForID(long)
	 */
	@Override
	public BookData getForID(final long id) {
		return bookRepository.findById(id).map(bookConverter::convert).orElse(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.BookFacade#findByIsbn(java.lang.String)
	 */
	@Override
	public BookData findByIsbn(final String isbn) {
		return bookConverter.convert(bookRepository.findByIsbn(isbn));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.BookFacade#findByAuthor(java.lang.String)
	 */
	@Override
	public List<BookData> findByAuthor(final String author) {
		return bookConverter.convertAll(bookRepository.findByAuthor(author));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.BookFacade#findByTitle(java.lang.String)
	 */
	@Override
	public List<BookData> findByTitle(final String title) {
		return bookConverter.convertAll(bookRepository.findByTitle(title));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.canoo.test.facade.BookFacade#create(com.canoo.test.facade.data.BookData)
	 */
	@Override
	public BookData create(final BookData bookData) {
		var currentUserName = getCurrentUserName();
		if (StringUtils.isNotBlank(currentUserName)) {
			return userRepository.findByUsername(currentUserName) //
					.map(user -> {
						var book = new Book();
						book.setAddedBy(user);
						reverseBookPopulator.populate(bookData, book);
						return bookConverter.convert(bookRepository.save(book));
					}).orElse(null);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.canoo.test.facade.BookFacade#update(com.canoo.test.facade.data.BookData)
	 */
	@Override
	public BookData update(final BookData bookData) {
		var book = bookRepository.findById(bookData.getId());
		book.ifPresent(entity -> {
			entity.setTitle(bookData.getTitle());
			reverseBookPopulator.populate(bookData, entity);
			bookRepository.save(entity);
		});

		return book == null ? null : getForID(bookData.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.canoo.test.facade.BookFacade#delete(com.canoo.test.facade.data.BookData)
	 */
	@Override
	public void delete(final BookData bookData) {
		var bookOp = bookRepository.findById(bookData.getId());
		bookOp.ifPresent(book -> {
			var currentUserName = getCurrentUserName();
			if (StringUtils.isNotBlank(currentUserName)) {
				userRepository.findByUsername(currentUserName) //
						.ifPresent(currentUser -> {
							if (currentUser.equals(book.getAddedBy())) {
								bookRepository.delete(book);
							}
						});
			}
		});
	}

	protected String getCurrentUserName() {
		var authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			LOG.info("\nprincipal: {}\n", authentication.getPrincipal());
			return authentication.getName();
		}
		return null;
	}

}
