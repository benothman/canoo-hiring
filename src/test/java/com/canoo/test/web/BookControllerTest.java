/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.web;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.canoo.test.core.model.Book;
import com.canoo.test.core.model.User;
import com.canoo.test.core.repository.BookRepository;
import com.canoo.test.core.repository.UserRepository;
import com.canoo.test.facade.converters.Converter;
import com.canoo.test.facade.data.BookData;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * {@code BookControllerTest}
 * <p/>
 * 
 * Created on 28 Aug 2018 at 13:50:14
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({ "junit" })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration
@WebAppConfiguration
public class BookControllerTest {

	@Autowired
	private WebApplicationContext context;
	@Resource
	private UserRepository userRepository;
	@Resource
	private BookRepository bookRepository;
	@Resource
	private Converter<Book, BookData> bookConverter;
	@Resource
	private Converter<BookData, Book> reverseBookConverter;

	private MockMvc mvc;
	private long number;
	private Book book;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		number = new Random().nextLong();
		book = bookRepository.save(createBook(userRepository.findByUsername("user1").get()));
	}

	@Test
	public void testGetAnonymous() throws Exception {
		mvc.perform(get("/books")).andExpect(status().is3xxRedirection());
	}

	@WithMockUser(username = "admin", password = "nimda", roles = { "ADMIN" })
	@Test
	public void testGetAuthenticated() throws Exception {
		mvc.perform(get("/books")) //
				.andDo(handler -> System.out.println(handler.getResponse().getContentAsString())) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
	}

	@Test
	@WithMockUser(username = "user1", password = "mypass1", roles = { "USER" })
	public void testGetByPK() throws Exception {
		mvc.perform(get("/books/1")) //
				.andDo(handler -> System.out.println(handler.getResponse().getContentAsString())) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
	}

	@Test
	@WithMockUser(username = "user1", password = "mypass1", roles = { "USER" })
	public void testGetByISBN() throws Exception {
		mvc.perform(get("/books/" + book.getIsbn())) //
				.andDo(handler -> System.out.println(handler.getResponse().getContentAsString())) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void testPost() throws Exception {
		var bookData = bookConverter.convert(book);
		bookData.setId(-1);
		bookData.setIsbn(bookData.getIsbn() + "-sjdksd");
		ObjectMapper mapper = new ObjectMapper();
		mvc.perform(post("/books") //
				.contentType(MediaType.APPLICATION_JSON_UTF8) //
				.with(csrf()) //
				.content(mapper.writeValueAsString(bookData))) //
				.andDo(System.out::println) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
	}

	@Test
	@WithUserDetails(value = "user1", userDetailsServiceBeanName = "userDetailsService")
	public void testPut() throws Exception {
		var bookData = bookConverter.convert(book);
		bookData.setIsbn(bookData.getIsbn() + "-sjdksd");
		bookData.setTitle(bookData.getIsbn());
		ObjectMapper mapper = new ObjectMapper();
		mvc.perform(put("/books/" + book.getId()) //
				.with(csrf()) //
				.contentType(MediaType.APPLICATION_JSON_UTF8) //
				.content(mapper.writeValueAsString(bookData))) //
				.andDo(handler -> System.out.println(handler.getResponse().getContentAsString())) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void testDeleteAdmin() throws Exception {
		mvc.perform(delete("/books/" + book.getId()).with(csrf())).andExpect(status().isOk());
	}

	@Test
	@WithUserDetails(value = "user2", userDetailsServiceBeanName = "userDetailsService")
	public void testDeleteUser() throws Exception {
		mvc.perform(delete("/books/" + book.getId()).with(csrf())).andExpect(status().is4xxClientError());
	}

	private Book createBook(final User addedBy) {
		var book = new Book();
		book.setAddedBy(addedBy);
		book.setAuthor("Me");
		book.setIsbn("ISBN-" + number);
		book.setTitle("Very nice book to read #" + number);
		book.setLanguage("English");
		book.setDescription("This is a very nice book to read about Me!");
		return book;
	}

}
