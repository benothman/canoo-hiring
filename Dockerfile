FROM openjdk:10-jdk
ARG JAR_FILE

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS JAVA_OPTS="" APP_SLEEP=1 

# Add a canoo user to run our application so that it doesn't need to run as root
RUN useradd -ms /bin/bash canoo
WORKDIR /home/canoo

ADD entrypoint.sh entrypoint.sh
RUN chmod 755 entrypoint.sh && chown canoo:canoo entrypoint.sh
USER canoo

ADD ${JAR_FILE} canoo-app.jar

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 8443
