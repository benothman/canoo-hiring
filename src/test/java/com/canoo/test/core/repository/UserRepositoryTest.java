/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import javax.annotation.Resource;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.canoo.test.core.model.User;
import com.canoo.test.core.model.UserRole;

/**
 * {@code UserRepositoryTest}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 23:19:19
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({ "junit" })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRepositoryTest {

	@Resource
	private UserRepository userRepository;

	@Test
	public void testSave() {
		var username = "admin" + (new Random().nextLong());
		var admin = userRepository
				.save(createUser("Admin User", username, username + "@book-library.com", "nimda", UserRole.ROLE_ADMIN));
		assertNotNull("The user must be not null", admin);
		assertFalse("The user PK must be not null", admin.isNew());
	}

	@Test
	public void testFindByUsername() {
		var username = "user" + (new Random().nextLong());
		userRepository
				.save(createUser("Normal User", username, username + "@book-library.com", "mypass", UserRole.ROLE_USER));
		var user = userRepository.findByUsername(username);
		assertNotNull("The user must be not null", user);
		assertTrue("The user must be not null", user.isPresent());
	}

	@Test
	public void testFindByEmail() {
		var username = "user" + (new Random().nextLong());
		userRepository
				.save(createUser("Normal User", username, username + "@book-library.com", "mypass", UserRole.ROLE_USER));
		var user = userRepository.findByEmail(username + "@book-library.com");
		assertNotNull("The user must be not null", user);
		assertTrue("The user must be not null", user.isPresent());
	}

	@Test
	public void testFindByRole() {
		var username = "user" + (new Random().nextLong());
		userRepository
				.save(createUser("Normal User", username, username + "@book-library.com", "mypass", UserRole.ROLE_USER));
		var users = userRepository.findByRole(UserRole.ROLE_USER);
		assertNotNull("The user list must be not null", users);
		assertFalse("The users list must be non empty", users.isEmpty());
	}

	@Test
	public void testDelete() {
		var username = "user" + (new Random().nextLong());
		var user = userRepository
				.save(createUser("Normal User", username, username + "@book-library.com", "mypass", UserRole.ROLE_USER));
		assertNotNull("The user must be not null", user);
		assertFalse("The user PK must be not null", user.isNew());
		userRepository.delete(user);
		assertNotNull("The optional object must be not null", userRepository.findByUsername(username));
		assertFalse("The optional must be empty", userRepository.findByUsername(username).isPresent());

	}

	private User createUser(final String name, final String username, final String email, final String password,
			final UserRole role) {
		var user = new User();
		user.setUsername(username);
		user.setEmail(email);
		user.setRole(role);
		user.setName(name);
		user.setPassword(password);
		return user;
	}

}
