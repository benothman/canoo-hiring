/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.canoo.test.core.model.User;
import com.canoo.test.core.model.UserRole;

/**
 * {@code UserRepository}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 18:30:41
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public interface UserRepository extends JpaRepository<User, Long> {

	/**
	 * Find the user by its username
	 * 
	 * @param username the username
	 * @return the use having the specified username
	 */
	public Optional<User> findByUsername(final String username);

	/**
	 * Find the user by its email
	 * 
	 * @param email the email
	 * @return the use having the specified email
	 */
	public Optional<User> findByEmail(final String email);

	/**
	 * Find the users with the given role.
	 * 
	 * @param role the role
	 * @return a list of users having the specified role.
	 */
	public List<User> findByRole(final UserRole role);

}
