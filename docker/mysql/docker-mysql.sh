#!/bin/sh
# ----------------------------------------------------------------------------
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
# ----------------------------------------------------------------------------


# stop the existing container if it exists
#docker stop mysqlserver
app="mysqlserver"
if docker ps -a | awk -v app="$app" 'NR > 1 && $NF == app{ret=1; exit} END{exit !ret}'; then
  docker stop "$app" && docker rm -f "$app"
fi


# start the container
docker run --name=$app -d -h $app \
	--network canoo_net \
	-p 3306:3006 \
	-e MYSQL_ROOT_PASSWORD=canoo123456 \
	-e MYSQL_USER=canoo_usr \
 	-e MYSQL_PASSWORD=canoo123456 \
	-v mysql-init.sql:/docker-entrypoint-initdb.d \
	mysql/mysql-server:5.7
       
sleep 3
