#!/bin/sh
# ----------------------------------------------------------------------------
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
# ----------------------------------------------------------------------------


PROFILE=$1

if [ "x$PROFILE" = "x" ]; then
	#PROFILE="default"
	PROFILE="docker"
fi

if [ "$PROFILE" = "default" ]; then
	java -jar target/canoo-hiring-test-0.0.1.jar
elif [ "$PROFILE" = "docker" ]; then
	CONTAINER_NAME="hiring-test"

	# Stop the existing container if it already exists
	docker stop $CONTAINER_NAME

	# Remove the existing container if it already exists
	docker rm $CONTAINER_NAME

	# Start the new container
	docker run -e "SPRING_PROFILES_ACTIVE=$PROFILE" --network canoo_net --name=$CONTAINER_NAME -d -p 8443:8443 -t canoo/$CONTAINER_NAME:latest

	sleep 3

	# connect the application to the default bridge
	docker network connect bridge $CONTAINER_NAME

else
	printf "ERROR: unknow profile name: $PROFILE\n"
fi	
