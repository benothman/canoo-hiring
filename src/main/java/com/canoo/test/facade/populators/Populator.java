/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.populators;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;

/**
 * {@code Populator}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 21:09:19
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public interface Populator<S, T> {

	/**
	 * Populate the target with data from the source
	 * 
	 * @param source the source object
	 * @param target the target object which will be populated from the source
	 *               object.
	 */
	public void populate(final S source, final T target);

	/**
	 * 
	 */
	@PostConstruct
	default void postConstruct() {
		LogManager.getLogger(getClass()).info("  --> initializing bean of type: {}", getClass().getName());
	}
}
