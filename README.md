/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */


The project is build using maven and compiled with JDK-10.
The build process creates also a Docker image which can be executed on 
a Docker container. To execute the code successfully please follow the
steps below:

1) Install JDK-10 which can be downloaded from the following URL:
   http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html

2) Install Maven which can be downloaded from the following URL:
   https://maven.apache.org/download.cgi

3) If you would like to run the project into a Docker container, please ensure that
   Docker is installed and running. Otherwise, it can be downloaded from the link
   below and then installed.
   https://www.docker.com/products/docker-desktop

4) Using the command line, navigate project folder and run the build script
   « project_dir$ ./build.sh »

5) Before proceeding with running the project, a MySQL database must be set up first.
   ( can be skipped if you want to use Apache Derby DB )
  a) under the folder « docker/mysql », there are few useful scripts which can be used
     set up the MySQL database
     i-   « docker-mysql.sh » to run the MySQL server on a docker container
     ii-  « mysql-connect.sh » is to connect to the MySQL server running on the docker container
     iii- « mysql-init.sql » is to initialise the database with a dedicated user.
  b) The project configuration file « application.yml » contains multiple spring profiles
     junit profile if for JUnit testing  (uses Apache Derby DB)
     eclipse profile is to run the project from Eclipse IDE (uses Apache Derby DB)
     docker is used to run the project in a docker container (use MySQL database running on Docker container)
     default can be used when running directly the jar file from the command line (use MySQL database running on localhost)

6) The script « docker-init.sh » can be used to run and setup the docker container for the MySQL server.

7) Once the Docker container is setup, the script « run.sh » can be used to run the project on
   a Docker container

8) If you decide to not use Docker, you can use the following command to run the project
   « java -jar target/canoo-hiring-test-0.0.1.jar »

