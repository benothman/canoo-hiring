/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

/**
 * {@code AbstractEntity}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 18:22:41
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity implements PersistentEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PK")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;
	@NotNull
	@CreatedDate
	@Column(name = "CREATION_TS", nullable = false, updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createTime;
	@NotNull
	@LastModifiedDate
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_TS", nullable = false)
	private Date modifiedTime;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.core.model.PersistentEntity#isNew()
	 */
	@Override
	public boolean isNew() {
		return (this.id == null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, "createTime", "modifiedTime");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, "createTime", "modifiedTime");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

	@PrePersist
	protected void onInsert() {
		if (this.createTime == null) {
			this.createTime = new Date();
		}
		if (this.modifiedTime == null) {
			this.modifiedTime = this.createTime;
		}
	}

	@PreUpdate
	protected void onUpdate() {
		this.modifiedTime = new Date();
	}
}
