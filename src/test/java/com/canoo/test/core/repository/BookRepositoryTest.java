/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Random;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.canoo.test.core.model.Book;
import com.canoo.test.core.model.User;
import com.canoo.test.core.model.UserRole;

/**
 * {@code UserRepositoryTest}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 23:19:19
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({ "junit" })
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookRepositoryTest {

	private static final String[] LANGUAGES = { "en", "de", "it", "fr", "ar" };
	private static final String ISBN_PREFIX = "ISBN-";

	@Resource
	private UserRepository userRepository;
	@Resource
	private BookRepository bookRepository;

	private long number;

	@Before
	public void setUp() {
		number = new Random().nextLong();
	}

	@Test
	public void test1Save() {
		var book = bookRepository.save(createBook());
		assertNotNull("The object must be not null", book);
		assertFalse("The PK must be not null", book.isNew());
	}

	@Test
	public void test2FindByIsbn() {
		var book = bookRepository.save(createBook());
		assertNotNull("The object must be not null", book);
		assertFalse("The PK must be not null", book.isNew());
		assertNotNull("The object must be not null", bookRepository.findByIsbn(ISBN_PREFIX + number));
	}

	@Test
	public void test3FindByAuthor() {
		var book = bookRepository.save(createBook());
		assertNotNull("The object must be not null", book);
		assertFalse("The PK must be not null", book.isNew());
		var books = bookRepository.findByAuthor("Author" + number);
		assertNotNull("The object must be not null", books);
		assertFalse("The list should not be empty", books.isEmpty());
	}

	@Test
	public void test4FindByTitle() {
		bookRepository.save(createBook());
		var books = bookRepository.findByTitle("Nice book to read");
		assertNotNull("The object must be not null", books);
		assertFalse("The list should not be empty", books.isEmpty());
	}

	@Test
	public void testDelete() {
		var book = bookRepository.save(createBook());
		assertNotNull("The object must be not null", book);
		assertFalse("The PK must be not null", book.isNew());
		// Delete
		bookRepository.delete(book);
		assertNull("The object must be null", bookRepository.findByIsbn(ISBN_PREFIX + number));
	}

	private User createUser() {
		return createUser("Name #" + number, "user" + number, "user" + number + "@domain.com",
				"randompassword" + number, UserRole.ROLE_USER);
	}

	private User createUser(final String name, final String username, final String email, final String password,
			final UserRole role) {
		var user = new User();
		user.setUsername(username);
		user.setEmail(email);
		user.setRole(role);
		user.setName(name);
		user.setPassword(password);
		return userRepository.save(user);
	}

	private Book createBook() {
		var book = new Book();
		book.setAuthor("Author" + number);
		book.setIsbn(ISBN_PREFIX + number);
		book.setTitle("Nice book to read");
		var index = Math.abs(new Random().nextInt(LANGUAGES.length));
		book.setLanguage(LANGUAGES[index]);
		book.setDescription("This book helps you understanding the basics of programming with Java");
		book.setAddedBy(createUser());
		return book;
	}
}
