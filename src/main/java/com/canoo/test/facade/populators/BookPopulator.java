/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.populators;

import org.springframework.stereotype.Component;

import com.canoo.test.core.model.Book;
import com.canoo.test.facade.data.BookData;

/**
 * {@code BookPopulator}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 21:09:19
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@Component("bookPopulator")
public class BookPopulator implements Populator<Book, BookData> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.facade.populators.Populator#populate(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void populate(final Book source, final BookData target) {
		target.setId(source.getId());
		target.setTitle(source.getTitle());
		target.setIsbn(source.getIsbn());
		target.setDescription(source.getDescription());
		target.setLanguage(source.getLanguage());
		target.setAddedBy(source.getAddedBy().getName());
		target.setAuthor(source.getAuthor());
		target.setCreateTime(source.getCreateTime());
		target.setUpdateTime(source.getModifiedTime());
	}
}
