/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.converters;

/**
 * {@code ConversionException}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 19:08:07
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
public class ConversionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5078650143526855059L;

	/**
	 * Creates new instance of {@code ConversionException}
	 */
	public ConversionException() {
		super();
	}

	/**
	 * Creates new instance of {@code ConversionException}
	 * 
	 * @param message
	 */
	public ConversionException(final String message) {
		super(message);
	}

	/**
	 * Creates new instance of {@code ConversionException}
	 * 
	 * @param cause
	 */
	public ConversionException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Creates new instance of {@code ConversionException}
	 * 
	 * @param message
	 * @param cause
	 */
	public ConversionException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
