/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.facade.converters;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * {@code Converter}
 * <p/>
 * 
 * 
 * Created on 27 Aug 2018 at 19:09:14
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 * 
 * @param <S> the source type
 * @param <T> the target type
 */
public interface Converter<S, T> extends org.springframework.core.convert.converter.Converter<S, T> {

	/**
	 * Convert the source item of type {@code S} by populating the target item of
	 * type {@code T}
	 * 
	 * @param source the source item
	 * @param target the target data item.
	 * @return the target item.
	 * @throws ConversionException
	 */
	public T convert(final S source, final T target) throws ConversionException;

	/**
	 * Convert all elements in the sources collection.
	 * 
	 * @param sources the source collection to be converted.
	 * @return a list of target items.
	 * @throws ConversionException
	 * @see #convertAllIgnoreExceptions
	 */
	default List<T> convertAll(final Collection<? extends S> sources) throws ConversionException {
		return CollectionUtils.isEmpty(sources) ? Collections.<T>emptyList()
				: sources.stream().map(this::convert).collect(Collectors.toList());
	}

	/**
	 * Convert all elements in the sources collection.
	 * 
	 * @param sources the source collection to be converted.
	 * @return a list of target items.
	 * @throws ConversionException
	 * @see #convertAllIgnoreExceptions
	 */
	default Set<T> convertAllSet(final Collection<? extends S> sources) throws ConversionException {
		return CollectionUtils.isEmpty(sources) ? Collections.<T>emptySet()
				: sources.stream().map(this::convert).collect(Collectors.toSet());
	}

	/**
	 * Convert all elements in the sources collection and ignore all exceptions.
	 * 
	 * @param sources the source collection to be converted.
	 * @return a list of target items.
	 */
	default List<T> convertAllIgnoreExceptions(final Collection<? extends S> sources) {
		return CollectionUtils.isEmpty(sources) ? Collections.emptyList() : sources.stream().map(source -> {
			try {
				return convert(source);
			} catch (final ConversionException ce) {
				getLogger().error("Conversion error occur during bean conversion", ce);
				return null;
			}
		}).filter(t -> t != null).collect(Collectors.toList());
	}

	/**
	 * @return the class logger
	 */
	default Logger getLogger() {
		return LogManager.getLogger(getClass());
	}
}
