/**
 * Copyright 2018, Nabil Benothman, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.canoo.test.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import lombok.Getter;
import lombok.Setter;

/**
 * {@code Book}
 * <p/>
 * 
 * Created on 27 Aug 2018 at 18:22:41
 *
 * @author <a href="mailto:nabil.benothman@gmail.com">Nabil Benothman</a>
 */
@Getter
@Setter
@Entity
@Table(name = "BOOKS")
public class Book extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6363811627042551920L;

	@NotBlank
	@Column(name = "BTITLE", nullable = false)
	private String title;
	@NotBlank
	@Column(name = "BAUTHOR", nullable = false)
	private String author;
	@NotBlank
	@Column(name = "BISBN", nullable = false, unique = true)
	private String isbn;
	@Column(name = "DESCRIPTION", length = 500)
	private String description;
	@NotBlank
	@Column(name = "BLANG", nullable = false)
	private String language;
	@ManyToOne(optional = false)
	@JoinColumn(name = "ADDEDBY_PK", referencedColumnName = "PK")
	private User addedBy;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.core.model.AbstractEntity#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, "createTime", "modifiedTime", "description", "addedBy");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.canoo.test.core.model.AbstractEntity#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, "createTime", "modifiedTime", "description", "addedBy");
	}
}
